%include "words.inc"
%include "lib.inc"
extern find_word

%define BUF_SIZE 255

global _start

section .rodata

overflow_error: db "overflow of buffer", 0
key_exist_error: db "this key doesn't exists",0

section .data

buf: times BUF_SIZE db 0

section .text

_start:
	mov rdi, buf
	mov rsi, BUF_SIZE
	
	call read_word ;Ввод ключа
		
	cmp rax, 0
	je .overflow
	
	mov rdi, rax ;записал ключ
	
	mov rsi, first_word ;записал указатель на словарь
	
	call find_word
	
	test rax, rax
	je .key_error
	
	.succes:
		mov rdi, rax
		add rdi, rdx
		add rdi, 8
		
		call string_length
		add rdi, rax
		add rdi, 1
		
		call print_string
		jmp .end
	
	.overflow:
		mov rdi, overflow_error
		jmp .error
	
	.key_error:
		mov rdi, key_exist_error
	.error:
		call print_error
	.end:
		call print_newline
		call exit
		
	
