NM=nasm -felf64 -o

main: main.o dict.o lib.o
	ld -o $@ $^
	
main.o: main.asm dict.o colon.inc words.inc
	$(NM) $@ $<
	
dict.o: dict.asm lib.inc
	$(NM) $@ $<
	
lib.o: lib.asm
	$(NM) $@ $<
	
clean : 
	rm -rf *.o
	rm -rf main
