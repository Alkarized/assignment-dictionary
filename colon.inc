%define ELEM 0
%macro colon 2
	%ifid %2
		%2: 
			dq ELEM

	%else 
		%fatal "incorrect label!"		
	%endif

	%ifstr %1
		db %1, 0

	%else 
		%fatal "incorrect key!"
	%endif

	%define ELEM %2	
%endmacro
