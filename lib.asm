global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte[rdi + rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	
    mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	mov rdx, 1
	syscall
	
	pop rdi
	
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	jmp print_char
	
	
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:

	mov rsi, 10
	mov rax, rdi
	mov r10, rsp
	
	dec rsp
	mov byte[rsp], 0
.divs:
	xor rdx, rdx
	div rsi
	add rdx, '0'
	dec rsp
	mov byte[rsp], dl
	cmp rax, 0
	je .print
	jmp .divs
	
.print:
	mov rdi, rsp
	call print_string
	mov rsp, r10
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge .print
    push rdi
	
	mov rdi, '-'
	call print_char
	
	pop rdi
	neg rdi
.print:
	jmp print_uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov rax, 1
.loop:
	mov dl, byte[rdi]
	cmp byte[rsi], dl
	jne .error
	
	cmp dl, 0
	je .end
	
	inc rdi
	inc rsi
	
	jmp .loop
	
.error:
	xor rax, rax
.end:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	
    xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	
	pop rax	
	
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r9
	push rdi
	xor r9, r9
	xor rdx, rdx
.read_space_symbol:
	push rdi
	push rdx
	push rsi
	
	call read_char
	
	pop rsi
	pop rdx
	pop rdi
	
	cmp r9, 1				;check if space symbol was already
	je .write_symbol
	
	cmp rax, ' '			;
	je .read_space_symbol   ;check for space symbol,
	cmp rax, 0x9   		    ;if it space symbol =>
	je .read_space_symbol   ;read char again until it not a space sybmol 
	cmp rax, 0xA            ;
	je .read_space_symbol   ;
							
	mov r9, 1				;get not a space symbol, so i will skip all space symbols 
	
	
.write_symbol:
	cmp rax, 0				
	je .done
	
	cmp rax, ' '			
	je .done   
	cmp rax, 0x9   		    
	je .done   
	cmp rax, 0xA           
	je .done 

	cmp rsi, rdx
	je .error
	
	inc rdx
	mov [rdi], rax
	inc rdi
	jmp .read_space_symbol
	
.done:
	inc rdi
	mov byte[rdi], 0
	pop rax
	jmp .end
.error: 
	pop rdi
	xor rax, rax
.end:
	pop r9
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push rdi
	mov rsi, 10
	xor rax, rax
	xor rdx, rdx
	
.check:
	cmp byte[rdi], '/'
	jle .end
	cmp byte[rdi], ':'
	jge .end
	
	push rdx
	mul rsi
	pop rdx
		
    add al, byte[rdi]
    sub al, '0'

	inc rdi
	inc rdx
	
	jmp .check
	
.end:
	pop rdi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	jne .positive
	
	inc rdi 
	call parse_uint
	cmp rdx, 0
	je .end
	
	neg rax
	inc rdx
	jmp .end 
	
.positive:
	jmp parse_uint
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:

	call string_length	
	cmp rdx, rax
	jl .not_eq

.loop:	
	mov rdx, [rdi]
	mov [rsi], rdx

	cmp byte[rdi], 0
	je .end
	
	inc rdi
	inc rsi
	
	jmp .loop
.not_eq:
	xor rax, rax	
.end:
	ret

; rdi - указатель на строку с ошибкой
print_error:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
	ret
