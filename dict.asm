%include "lib.inc"

global find_word

%define ELEM 8
 
section .text
			;rdi - ключ, rsi - указатель на словарь, 
			;rax - конечное значение (0 - если не найдено иначе адрес словаря, где расположено значение по ключу)
find_word:  
	.find:
		test rsi,rsi
		je .error
		
		add rsi, ELEM
		
		push rdi
		push rsi
		
		call string_equals
		
		pop rsi
		pop rdi
		
		test rax, rax
		jne .succes
		
		sub rsi, ELEM
		mov rsi, [rsi]
		
		jmp .find
		
	.error:
		xor rax, rax
		ret
		
	.succes:
		sub rsi, ELEM
		mov rax, rsi
		ret
